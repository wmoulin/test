#!/usr/bin/env bash
set -e
xvfb-run --server-args="-screen 0, 1366x768x24" npm run selenium-start
